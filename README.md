# Overleaf Utils

Scripts to interact with self-hosted Overleaf servers. Currently:

- List all project names and IDs
- Download specified projects from IDs

## Install

This repository only relies on one external library: `aiohttp`. To install it:
```bash
sudo pip install --system aiohttp
```
and you should be good to go.

## Cookie

The connection with overleaf is done with a session cookie.
To get it, follow the following guides.
It should be of the form `s%3APUHN9SpwTpLpJt6z9cZPnihew0XX5rY5.WFQ8%2FfStd9Axu6iALScsVTuT5Lh5%2FxahEuGm4sly3Yo`.
Once retrieved, put the cookie in the environment variable `SHARELATEX_SID`:

```bash
export SHARELATEX_SID='s%3APUHN9SpwTpLpJt6z9cZPnihew0XX5rY5.WFQ8%2FfStd9Axu6iALScsVTuT5Lh5%2FxahEuGm4sly3Yo'
```

#### Retrieve In Firefox

![Steps for Firefox cookie retrieval](cookie-firefox.jpg)

0. Open the developper console: F12
1. Go in the "storage" tab
2. Go in the "cookie" section
3. Select the "value" associated with "sharelatex.sid"

## Auto-Sync
An auto synchronization can be setup. Using a timer (and a systemd service), the script `sync_overleaf_git.sh` can be triggered to
- Download speficied projects
- Look for change
- Update specified git repositories with changes if there is any

This tool can be installed on servers (e.g. iago used at Heudiasyc) or personal machines.

### Configure which projects to sync
The script needs to know which project to save where. For this, a `projects.csv` file must be created. Its form is
```csv
OVERLEAFPROJECTID,namespace/repository.git
```

To get the `OVERLEAFPROJECTID` you can use `list_projects.py`.
For example, if one wants to create a `documentation` repository in the `hds_vehint` group, `NAMESPACE/REPOSITORY.git` will look like `hds_vehint/documentation.git`.
Repositories are created with private access if they dont already exist. Just make sure the NAMESPACE exists.

### Set up Git access
By default, `sync_overleaf_git.sh` uses the user's SSH key to pull and push.
Instead it can use a [Group/Project bot](https://gitlab.utc.fr/help/user/group/settings/group_access_tokens.md) with an access token.
For this:
1. Go in your group or project settings, under "Access Tokens"
2. Add a token with the name and expiration date of your choice (infinite token can also be used), select the "Maintainer" role, tick the "read_repository" and "write_repository" options, create the token and copy it
3. Retrieve the bot name by going in the Group/Project "Members". It should look like `group_xxxxx_bot`

### Create the timer
Create the environment file `sync_overleaf_git.env` in this README's folder and put inside:
- The overleaf token retrieved in [Cookie](#cookie)
- The absolute path to `projects.csv` created in [Configure which projects to sync](#configure-which-projects-to-sync)
- (Optionnally) The git token retrieved in [Set up Git access](#set-up-git-access)

So that it looks like
```bash
SHARELATEX_SID="s%3APUHN9SpwTpLpJt6z9cZPnihew0XX5rY5.WFQ8%2FfStd9Axu6iALScsVTuT5Lh5%2FxahEuGm4sly3Yo"
PATH_PROJECTS_CSV="/path/to/projects.csv"
GIT_USER_AND_TOKEN="group_xxxxx_bot:xxxxxxxxxxxxxxxxxxxxxxxxxx"
```

Finally, make the timer and service visible and runnable, do:
```bash
mkdir -p  ~/.config/systemd/user/
ln -s $(pwd)/sync_overleaf_git.sh ~/.local/bin/
ln -s $(pwd)/sync_overleaf_git.{env,timer,service} ~/.config/systemd/user/
systemctl --user start sync_overleaf_git.timer
```

### Customize the timer
The time between two synchronization is dictated by the `OnCalendar` field of `sync_overleaf_git.timer`.
It can be changed according to [systemd timer syntax](https://wiki.archlinux.org/title/Systemd/Timers#Realtime_timer).
