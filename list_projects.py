#! /usr/bin/env python3

import sys, json, asyncio
from bs4 import BeautifulSoup
from pathlib import Path
from urllib.parse import unquote
from argparse import ArgumentParser
from aiohttp import ClientSession

from common import get_overleaf_cookie, DEFAULT_OVERLEAF_HOST

def parse_args():
    parser = ArgumentParser(description="Lists all projects and their project IDs")
    parser.add_argument("--csv", help="Output as csv file instead of pretty print", action="store_true")
    parser.add_argument("--host", type=str, help="URL of the host from which to list projects", default=DEFAULT_OVERLEAF_HOST)
    return parser.parse_args()

async def get_html(host, cookie):
    async with ClientSession(cookies={"sharelatex.sid": cookie}) as session:
        async with session.get(f"{host}/project") as response:
            if response.status==200:
                return await response.text()
            else:
                print(f"Request returned {response.status} ({response.reason})", file=sys.stderr)

def parse_projects(html):
    # The projects are given as a json inside a <meta name="ol-prefetchedProjectsBlob">
    # so we need to first parse the html, then parse json inside of it
    soup = BeautifulSoup(html, "html.parser")
    projects_data_raw = soup.find("meta", attrs={"name": "ol-prefetchedProjectsBlob"})
    if not projects_data_raw:
        print("Received html does not look right, make sure your session ID is correct and still valid", file=sys.stderr)
        sys.exit(1)

    content = json.loads(projects_data_raw["content"])
    projects = content["projects"]

    projects_simple = { project["id"]: project["name"] for project in projects if not project["archived"] }
    return projects_simple

if __name__=="__main__":
    args = parse_args()
    cookie = get_overleaf_cookie()
    if cookie:
        html = asyncio.run(get_html(args.host, cookie))
        projects = parse_projects(html)
        for pid, name in projects.items():
            print(f"{pid},{name}")
