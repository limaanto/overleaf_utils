#! /usr/bin/env python3

import os, sys

def get_overleaf_cookie():
    if "SHARELATEX_SID" in os.environ:
        return os.environ["SHARELATEX_SID"]
    else:
        print("Environment variable 'SHARELATEX_SID' is not defined, connection with overleaf will not work. Refer to the README.md on how to retrieve that cookie", file=sys.stderr)

DEFAULT_OVERLEAF_HOST = "https://overleaf.hds.utc.fr"
