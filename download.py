#! /usr/bin/env python3

import os, sys, asyncio
from pathlib import Path
from zipfile import ZipFile
from argparse import ArgumentParser
from aiohttp import ClientSession

from common import get_overleaf_cookie, DEFAULT_OVERLEAF_HOST

def parse_args():
    parser = ArgumentParser(description="Downloads the given overleaf project and unzip it")
    parser.add_argument("--host", type=str, help="URL of the host from which to download", default=DEFAULT_OVERLEAF_HOST)
    parser.add_argument("-o", "--output", type=str, help="Directory where zip files are downloaded", default="/tmp")
    parser.add_argument("-u", "--unzip", help="Wether to unzip or not the downloaded projects", action="store_true")
    parser.add_argument("PROJECT_IDS", type=str, nargs="+", help="ID of the project to download ('overleaf.example.com/project/PROJECTID')")
    return parser.parse_args()

async def get_project(output_dir, session, host, project, unzip):
    response = await session.get(f"{host}/project/{project}/download/zip")
    if response.status==200:
        filename = response.content_disposition.filename

        # Hack because overleaf escapes latin1 characters as if they were utf-8
        # and playing with encoding was not working
        specialchars = {
            "\udce8": "è",
            "\udce9": "é",
        }

        for k, v in specialchars.items():
            filename = filename.replace(k, v)

        output_zip = output_dir / filename
        with open(output_zip, "wb") as f:
            async for chunk in response.content.iter_chunked(2**15):
                f.write(chunk)
        print(f"'{output_zip}' downloaded")
        if unzip:
            output_uncompressed_dir = output_zip.with_suffix("")
            with ZipFile(output_zip, "r") as f:
                f.extractall(output_uncompressed_dir)
            os.remove(output_zip)
            print(f"'{output_uncompressed_dir}' uncompressed")
    else:
        print(f"Request {project} returned {response.status} ({response.reason}):", file=sys.stderr)
        print(await response.read(), file=sys.stderr)
        sys.exit(1)

    return response.status

async def get_all_projects(output_dir, host, cookie, projects, unzip):
    async with ClientSession(cookies={"sharelatex.sid": cookie}) as session:
        statuses = await asyncio.gather(*[get_project(output_dir, session, host, project, unzip) for project in projects])

if __name__=='__main__':
    args = parse_args()
    cookie = get_overleaf_cookie()
    if cookie:
        output_dir = Path(args.output).expanduser().resolve(strict=True)
        asyncio.run(get_all_projects(output_dir, args.host, cookie, args.PROJECT_IDS, args.unzip))
