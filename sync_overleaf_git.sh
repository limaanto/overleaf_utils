#! /usr/bin/env bash

set -e

if [[ -z "$SHARELATEX_SID" ]]
then
    echo "Overleaf Sync Error: SHARELATEX_SID is not set, refer to the README.md for how to fix this" >&2
    exit 1
fi

if [[ -z "$PATH_PROJECTS_CSV" ]]
then
    echo "Overleaf Sync Error: PATH_PROJECTS_CSV is not set, refer to the README.md for how to fix this" >&2
    exit 1
fi

if [[ -z "$GIT_USER_AND_TOKEN" ]]
then
    GIT_HOST='git@gitlab.utc.fr:'
else
    GIT_HOST="https://$GIT_USER_AND_TOKEN@gitlab.utc.fr/"
fi

WORKING_DIR=$(mktemp -d)
OVERLEAF_UTILS_DIR=$(dirname $(realpath $0))

# Parse projects to be saved
declare -A REPOS
while IFS=, read -r PID REPO_PATH
do
    REPOS[$PID]=$REPO_PATH
done < $PATH_PROJECTS_CSV

# Parse all projects
python3 $OVERLEAF_UTILS_DIR/list_projects.py > $WORKING_DIR/projects.csv
declare -A PROJECTS
while IFS=, read -r PID PROJECT
do
    PROJECTS[$PID]=$PROJECT
done < $WORKING_DIR/projects.csv

# Clone git repos with their associated project name
for PID in "${!REPOS[@]}"
do
    git clone "$GIT_HOST${REPOS[$PID]}" -- "$WORKING_DIR/${PROJECTS[$PID]}" || \
    (
        mkdir -p -- "$WORKING_DIR/${PROJECTS[$PID]}" && \
        cd "$WORKING_DIR/${PROJECTS[$PID]}" && \
        git init && git remote add origin "$GIT_HOST${REPOS[$PID]}" && \
        curl https://www.toptal.com/developers/gitignore/api/latex > .gitignore
    )
done

# Download all projects directly in the right folders
python3 $OVERLEAF_UTILS_DIR/download.py --unzip --output=$WORKING_DIR ${!REPOS[@]}

# Go in each repo folder to add, commit and push
for PID in "${!REPOS[@]}"
do
    cd "$WORKING_DIR/${PROJECTS[$PID]}"
    git add .

    # If there is actually something to commit and push
    if [[ $(git status --porcelain | wc -l) > 0 ]]
    then
        git config user.name "Overleaf Synchronisation Bot"
        git config user.email "overleaf-sync@utc.fr"
        git commit --no-gpg-sign -m "Auto-Update"
        branch=$(git rev-parse --abbrev-ref HEAD)
        git push origin $branch
    else
        echo "${PROJECTS[$PID]} is up to date, skipping"
    fi
done

# Clean up
rm -rf -- "$WORKING_DIR"
